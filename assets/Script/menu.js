
cc.Class({
    extends: cc.Component,

    properties: {
        mysound:cc.AudioClip, 
    },
    start(){
        var val=cc.sys.localStorage.getItem('sound');
        if(val==="0"){
            cc.audioEngine.pauseAllEffects(); 
        }
    },

    OnBackToBeginnerClicked() {
        cc.audioEngine.play(this.mysound,false,0.4);
        cc.director.loadScene("beginner");
    },
   
    OnBackToBasicClicked() {
        cc.audioEngine.play(this.mysound,false,0.4);
        cc.director.loadScene("basic");
    },
    OnBackToMediumClicked() {
        cc.audioEngine.play(this.mysound,false,0.4);
        cc.director.loadScene("medium");
    },
    OnBackToAdvanceClicked() {
        cc.audioEngine.play(this.mysound,false,0.4);
        cc.director.loadScene("advance");
    },
    OnBackToStartClicked() {
        cc.audioEngine.play(this.mysound,false,0.4);
        cc.director.loadScene("start");
    }
});
