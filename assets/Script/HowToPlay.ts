// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.AudioClip)
    mysound:cc.AudioClip=null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    How2Play () {
        cc.sys.localStorage.setItem('h2p',JSON.stringify(1));
        cc.audioEngine.playEffect(this.mysound,false);
        cc.director.loadScene("Tutorial");
    }

    // update (dt) {}
}
