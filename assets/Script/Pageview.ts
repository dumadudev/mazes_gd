// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    pageV:cc.Node =null;

    @property(cc.Label)
    cont:cc.Label =null;

    @property(cc.Label)
    done:cc.Label =null;s

    @property(cc.AudioClip)
    mysound:cc.AudioClip=null;
   

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}
    onNextClick()
    {   
        
        if(this.cont.string==="DONE")
        {
            var once=cc.sys.localStorage.getItem('h2p');
            if(once==1){
                cc.director.loadScene("start");
                cc.audioEngine.playEffect(this.mysound,false);
            }
            else
            {
            cc.sys.localStorage.setItem('Learned',JSON.stringify(1));
            cc.director.loadScene("menu");
            cc.audioEngine.playEffect(this.mysound,false);
            }

           
        }
        else {
            this.pageV.x-=1080;
            cc.audioEngine.playEffect(this.mysound,false);
        }
        
        
        
        
    }

     update () {
        var val=cc.sys.localStorage.getItem('sound');
        var val1=cc.sys.localStorage.getItem('music');
        if(val==="0"){
            cc.audioEngine.pauseAllEffects(); 
        }
        console.log(this.node.getChildByName("view").getChildByName("content").x);
        if(this.pageV.x<=-2500)
        {
            //console.log(this.node.getChildByName("content").x);
            cc.sys.localStorage.setItem('Learned',JSON.stringify(1));
            this.cont.string = "DONE";
        //    cc.director.loadScene("game");
           
        }
        else{
            this.cont.string = "CONTINUE";
        }
    }

    // update (dt) {}
}
